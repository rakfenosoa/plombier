<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../../vendor/autoload.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = $_POST["name"];
    $firstname = $_POST["firstname"];
    $tel = $_POST["tel"];
    $email = $_POST["mail"];
    $date = $_POST["datepicker"];
    $time = $_POST["timepicker"];
    $besoins = $_POST["besoins"];

    // Production SMTP settings
    $smtp_server = 'localhost';  // Replace with your SMTP server address exemple: smtp.gmail.com
    $smtp_port = 1025; // Replace with your SMTP server port
    $smtp_username = 'your_username';  // Replace with your SMTP username
    $smtp_password = 'your_password';  // Replace with your SMTP password

    $mail = new PHPMailer(true);
    $mail->setLanguage('fr');

    try {
        // SMTP server settings for production
        $mail->isSMTP();
        $mail->Host = $smtp_server;
        $mail->SMTPAuth = true;
        $mail->Username = $smtp_username;
        $mail->Password = $smtp_password;
        $mail->Port = $smtp_port;

        // Adresses emails envoyeur / recepteur
        $mail->setFrom($email, $name);
        $mail->addAddress('aledplombier@gmail.com');

        $mail->Subject = "Fomulaire de contact rempli par $name";
        $mail->Body = "Nom: $name\nPrénom: $firstname\nTeléphone: $tel\nEmail: $email\nDate: $date\nHeure: $time\nBesoins:\n$besoins";

        // Envoi email
        $mail->send();
        echo "Merci de votre message. Nous reviendrons bientôt vers vous!";
    } catch (Exception $e) {
        echo "Oops! Quelque chose ne s'est pas passé comme prévu. Veuillez réessayer plus tard.<br><b>Erreur:</b> <u>{$mail->ErrorInfo}</u>";
    }
}
?>