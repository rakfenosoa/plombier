(function () {
  /*  $("[data-translate]").jqTranslate("../languages/demo"); */

  $("#widget").localizationTool({
    defaultLanguage: "fr_FR",
    showFlag: false,
    showLanguage: false,
    strings: {
      "class:accueilText": {
        fr_FR: "Accueil",
        he_IL: "ברוך הבא",
      },
      "class:servicesText": {
        fr_FR: "Services",
        he_IL: "שירותים",
      },
      "class:realisationsText": {
        he_IL: "הִתמַמְשׁוּת",
        fr_FR: "Réalisations",
      },
      "class:expertisesText": {
        he_IL: "מומחיות",
        fr_FR: "Expertises",
      },
      "class:temoignagesText": {
        he_IL: "המלצות",
        fr_FR: "Témoignages",
      },
      "class:prendreRendezVous": {
        he_IL: "לקבוע פגישה",
        fr_FR: "Prendre Rendez-vous",
      },
      "class:accueilTitre": {
        he_IL: "26 שנות ניסיון לשירותכם",
      },
      "class:accueilDescription": {
        he_IL: `במסירות ואהבה גדולה למקצוע שלי, אני עומד לשירותכם בפתרון בעיות האינסטלציה שלכם. תיקוני אינסטלציה, התקנה ותיקון דוודי שמש, מתן יעוץ ועוד.. כל זאת בהתחייבות לשירות אישי, אמין ומעולה.`,
      },
      "class:enSavoirPlus": {
        he_IL: `למד עוד`,
      },
      "class:mesServices": {
        he_IL: `השירותים שלי`,
      },
      "class:mesServicesDescriptions": {
        he_IL: `איתן, האינסטלטור שלך, מעמיד את המומחיות שלו לרשותך!`,
      },
      "class:installationText": {
        he_IL: `התקנה ותיקון דוודי שמש`,
      },
      "class:installationDescText": {
        he_IL: `תוך הקפדה על זהירות, תקנים וחלקים איכותיים ובהבטחה לעבודה מיטבית מתחילתה ועד סופה.`,
      },
      "class:reparationDepannageText": {
        he_IL: `לְתַקֵן
        ופתרון בעיות`,
      },
      "class:reparationEtText": {
        he_IL: `לְתַקֵן`,
      },
      "class:depannageText": {
        he_IL: `ופתרון בעיות`,
      },
      "class:reparationDepannageDescText": {
        he_IL: `במקרה של בעיות אינסטלציה בלתי צפויות כמו נזילות, צנרת חסומה או תקלות אחרות, אני מגיב במהירות בשירותי תיקון וטיפול בתקלות. מצויד לאבחן ולתקן ביעילות, בהיותי מוסמך, אני ממזער את ההפרעה לשגרת היום שלך.`,
      },
      "class:conseilExpertiseText": {
        he_IL: `עצות ו
        מומחיות`,
      },
      "class:conseilText": {
        he_IL: `מתן ייעוץ`,
      },
      "class:expertiseText": {
        //he_IL: `מומחיות`,
        he_IL: ``,
      },
      "class:conseilExpertiseDescText": {
        he_IL: `עבור פרויקט שיפוץ או השבחה שאתם עומדים בפניו, סמכו עליי שאדריך אתכם בהתאם לצרכים, תקציב וההעדפות שלכם כדי לשמר את הביצועים של מערכת האינסטלציה שלכם לאורך זמן.`,
      },
      "class:urgenceInterventionsText": {
        he_IL: `שירותי חירום 24 שעות ביממה`,
      },
      "class:urgenceText": {
        he_IL: `מצבי חירום והתערבות`,
      },
      "class:interventionsText": {
        he_IL: `24 שעות ביממה`,
      },
      "class:urgenceInterventionsDescText": {
        he_IL: `בגל שעות היום והלילה, נגיע אליכם בהקדם האפשרי.`,
      },
      "class:expertRechercheFuiteText": {
        he_IL: `איתור דליפות וחסימות`,
      },
      "class:expertRechercheFuiteDescText": {
        he_IL: `הודות למצלמה תרמית מתקדמת וציוד מצלמה לחקירת צינורות מתבצע איתור ואבחון מהיר ומדויק של מקור הבעיה.`,
      },
      "class:realisationTitreText": {
        he_IL: `העבודות שלי`,
      },
      "class:realisationDescriptionText": {
        he_IL: `חקור כמה מהפרויקטים וההישגים שלנו בתחום האינסטלציה, שביצענו בהצלחה.`,
      },
      "class:avantText": {
        he_IL: "לפני",
      },
      "class:apresText": {
        he_IL: "לאחר",
      },
      "class:scrollText": {
        he_IL: "גְלִילָה",
      },
      "class:pourquoiFaireAppelText": {
        he_IL: `חשיבות הזמנת מומחה לאינסטלציה`,
      },
      "class:pourquoiFaireAppelDescriptionText": {
        he_IL: `כל בעיה מצריכה ידע מדויק, הניסיון שלי כאינסטלטור מוסמך מעל 26 שנים מבטיח פתרונות יעילים, ארוכי טווח וללא דופי.`,
      },
      "class:expertTechniqueText": {
        he_IL: `מומחיות טכנית`,
      },
      "class:expertTechniqueDescText": {
        he_IL: `בעיות אינסטלציה יכולות להיות מורכבות ומגוונות, פנייה למומחה לירושה להינות ממומחיות טכנית מעמיקה לאבחון מדויק ולפתרון יעיל בכל סוג של תקלה.`,
      },
      "class:qualiteDurableText": {
        he_IL: `איכות`,
      },
      "class:qualiteDurableDescText": {
        he_IL: `איש מקצוע מוסמך בתחום האינסטלציה מבטיח תיקונים והתקנות באיכות גבוהה ובעמידה לטווח ארוך של המערכות והציוד.`,
      },
      "class:gainTempsText": {
        he_IL: `חסכון בזמן `,
      },
      "class:gainTempsDescText": {
        he_IL: `הימנע מטרחה וטעויות יקרות, מומחה אינסטלציה מוסמך יחסוך לך זמן ואנרגיה.`, 
      },
      "class:conseilsPertinentsText": {
        he_IL: `מתן יעוץ`,
      },
      "class:conseilsPertinentsDescText": {
        he_IL: `יעוץ לתחזוקה נכונה ושיפורים עתידיים. 
        יעוץ מותאם אישית יעזור לכם לקבל החלטות מושכלות לבריאות ארוכת הטווח של מערכת האינסטלציה שלכם.`,
      },
      "class:desClientsSatisfaitsText": {
        he_IL: `לקוחות מרוצים`,
      },
      "class:desClientsSatisfaitsDescText": {
        he_IL: `איכות ושביעות רצון זו המחוייבות שלי כלפי הלקוחות.`,
      },
      "class:jeanMarcText": {
        he_IL: `אינסטלטור יוצא דופן, אדם זמין במיוחד, איש מקצוע פרפקציוניסט ותמיד שם כשצריך אותו. עבודה ללא דופי ומתמשכת! אין לי מספיק מילים כדי לקבוע את זה בשבילי זה הכי טוב לא לא חשבתי שכיום עדיין יש אנשים כמוהו`,
      },
      "class:nomJeanMarc": {
        he_IL: `ז׳אן מארק`,
      },
      "class:daniText": {
        he_IL: `בידי זהב הוא מצדיק את המוניטין שלו, העניק לנו שירות אדיב ומקצועי. 
        פתרונות יצירתיים, אמינות ומספק את העבודה בזמן.`,
      },
      "class:nomDani": {
        he_IL: `דני`,
      },
      "class:raphaelText": {
        he_IL: `אנו ממליצים על איתן, ליצור קשר בראש סדר העדיפויות במידת הצורך. סיבות:
        1. ניסיונו המעורפל ומומחיותו המעמיקה.
        2. היעילות שלו בהשלמת כל פרויקט שהוא מבצע במהירות ובהצלחה רבה.
        3. הזמינות שלו לענות על שאלות ולייעץ ללקוחותיו.
        3. המחירים שלו נכונים לחלוטין`,
      },
      "class:nomRaphael": {
        he_IL: `רפאל`,
      },
      "class:sarahText": {
        he_IL: `רציני מאוד, מקצועי וקשוב ללקוח, נלהב מהעבודה שלו, ומחפש תמיד את השלמות.`,
      },
      "class:nomSarah": {
        he_IL: `שרה`,
      },
      "class:leaText": {
        he_IL: `לאחרונה השתמשתי בשירות של איתן ואני מאוד מרותה מהעבודה שלו, מאוד מקצועי, אמין וידידותי. 
        בנוסף בתום העבודה שלו לעוזרת לא היה מה לעשות מכיוון שהשאיר הכל נקי אחריו.`,
      },
      "class:nomLea": {
        he_IL: `לאה`,
      },
      "class:vanessaText": {
        he_IL: `תודה רבה על המקצועיות והכישורים שלך. אתה רציני מאוד, תודה רבה!`,
      },
      "class:nomVanessa":{he_IL: `ונסה`},
      "class:davidText": {
        he_IL: `אני ממליץ בחום על איתן השרברב על עבודתו שבוצעה בביתי בשל מהירותו, כישוריו וידידותיו.`,
      },
      "class:prendreRdvTitleText": {
        //he_IL: `לקחת קביעת  פגישה`,
        he_IL: `צור קשר`,
      },
      "class:prendreRdvDescText": {
        //he_IL: `ליהנות מפתרונות אינסטלציה מהירים ומקצועיים. אני מוכן לענות על הצרכים שלך ולספק לך חוויה ללא טרחה`,
        he_IL: ``,
      },
      "class:obtenirReponseText": {
       // he_IL: `כדי לקבל מאיתנו מענה מהיר אנא מלא טופס זה`,
        he_IL: ``,
      },
      "class:contactUrgenceText": {
        he_IL: `צור קשר למקרי חירום*`,
      },
      "class:contactUrgenceDescText": {
        he_IL: `זמינות בכל יום בכל עת למעט שבת, נדרשת עמלות ספציפיות, גישה והסכמה`,
      },
      "class:nomText": {
        he_IL: `שֵׁם`,
      },
      "class:prenomText": {
        he_IL: `שם פרטי`,
      },
      "class:telephoneText": {
        he_IL: `טלפון`,
      },
      "class:emailText": {
        he_IL: `אימייל`,
      },
      "class:dateText": {
        he_IL: `תַאֲרִיך`,
      },
      "class:heureText": {
        he_IL: `שָׁעָה`,
      },
      "class:besoinText": {
        he_IL: `מהות הפניה`,
      },
      "class:envoyerText": {
        he_IL: `שליחה`,
      },
    },
  });
})(jQuery);
