(function () {
  $(".custom-select, #dropdownMenuButton").each(function () {
    $(this).on("click", function () {
      $(this).toggleClass("open");
    });
  });

  $(".dropdown-menu").each(function () {
    $(this).click(function () {
      $(".custom-select, #dropdownMenuButton").removeClass("open");
    });
  });

  $(".sliding-link").click(function (e) {
    e.preventDefault();
    var aid = $(this).attr("href");
    $("html,body").animate({ scrollTop: $(aid).offset().top - 196 }, "slow");
  });

  $(".target").css({
    width: "61px",
    left: "490.98px",
    top: "117.29px",
  });
  $(".header .nav-link").each(function () {
    $(this).hover(function () {
      var currentPosition = $(this).position();
      var currentWidth = $(this).innerWidth();
      var currentHeight = $(this).innerHeight();

      $(".target").css({
        width: currentWidth,
        left: currentPosition.left + 60,
        top: currentPosition.top + currentHeight,
      });
    });
  });

  var sections = $(".content > .section-calculette"),
    nav = $(".header"),
    nav_height = nav.outerHeight();

  $(window).on("scroll", function () {
    var cur_pos = $(this).scrollTop();

    sections.each(function () {
      var top = $(this).offset().top - nav_height,
        bottom = top + $(this).outerHeight();

      if (cur_pos >= top && cur_pos <= bottom) {
        var $elMenu = nav.find('a[href="#' + $(this).attr("id") + '"]');

        nav.find(".nav-item").removeClass("active");

        $elMenu.parent().addClass("active");

        var currentPosition = $elMenu.position();
        var currentWidth = $elMenu.innerWidth();
        var currentHeight = $elMenu.innerHeight();

        $(".target").css({
          width: currentWidth,
          left: currentPosition.left + 60,
          top: currentPosition.top + currentHeight,
        });
      }
    });
  });
})(jQuery);
