(function () {
  function btnScroll($el, $prev, $next) {
    $el.on("afterChange", function (event, slick, currentSlide) {
      if (currentSlide === 0) {
        $prev.addClass("hide");
      } else {
        $prev.removeClass("hide");
      }

      if (slick.currentSlide >= slick.slideCount - slick.options.slidesToShow) {
        $next.addClass("hide");
      } else {
        $next.removeClass("hide");
      }
    });
  }
  function settingResp($slidesToShow, $slidesToScroll, $dots) {
    return {
      slidesToShow: $slidesToShow,
      slidesToScroll: $slidesToScroll,
      dots: $dots,
    };
  }

  function caroussel(
    $el,
    $slidesToShow,
    $prevArrow,
    $nextArrow,
    //$dots = true,
    $direction,
    $respSettings768,
    $respSettings576
  ) {
    $el
      .not(".slick-initialized")
      .slick({
        infinite: false,
        slidesToShow: $slidesToShow,
        slidesToScroll: 1,
        variableWidth: true,
        nextArrow: $nextArrow,
        prevArrow: $prevArrow,
        touchThreshold: 100,
        rtl: $direction,
        responsive: [
          {
            breakpoint: 1200,
            settings: settingResp(2, 1, true),
          },
          {
            breakpoint: 768,
            settings: $respSettings768,
          },
          {
            breakpoint: 576,
            settings: $respSettings576,
          },
        ],
      })
      .on("setPosition", function (event, slick) {
        slick.$slides.css("height", slick.$slideTrack.height() + "px");
      });
  }

  function destroyCarousel() {
    if ($("#realisations-carousel").hasClass("slick-initialized")) {
      $("#realisations-carousel").slick("unslick");
    }
    if ($("#realisations-expert-carousel").hasClass("slick-initialized")) {
      $("#realisations-expert-carousel").slick("unslick");
    }
    if ($("#temoignages-carousel").hasClass("slick-initialized")) {
      $("#temoignages-carousel").slick("unslick");
    }
    if ($("#services-carousel").hasClass("slick-initialized")) {
      $("#services-carousel").slick("unslick");
    }
  }

  function lanchCarousel($direction) {
    caroussel(
      $("#realisations-carousel"),
      2.5,
      ".real_previous_caro",
      ".real_next_caro",
      //true,
      $direction,
      settingResp(2, 1, true),
      settingResp(1, 1, true)
    );
    caroussel(
      $("#realisations-expert-carousel"),
      1.8,
      ".real_expert_previous_caro",
      ".real_expert_next_caro",
      //true,
      $direction,
      settingResp(2, 1, true),
      settingResp(1, 1, true)
    );
    caroussel(
      $("#temoignages-carousel"),
      1.15,
      ".testimonial_previous_caro",
      ".testimonial_next_caro",
      //false,
      $direction,
      settingResp(1, 1, true),
      settingResp(1, 1, true)
    );
    caroussel(
      $("#services-carousel"),
      3.5,
      ".service_previous_caro",
      ".service_next_caro",
      //true,
      $direction,
      settingResp(2, 1, true),
      settingResp(1, 1, true)
    );
  }

  function getAllLabelText() {
    $("label").each(function () {
      const $id = $(this).attr("for");

      if (localStorage.getItem("pl_language") === "fr_FR") {
        $(`#${$id}`).attr("placeholder", $(this).attr("data-placeholder"));
      } else if (localStorage.getItem("pl_language") === "he_IL") {
        $(`#${$id}`).attr("placeholder", $(this).text());
      }
      $(this).toggleClass("d-none");
    });
  }

  $(document).ready(function () {
    $(".ltool-language").each(function () {
      $(this).click(function () {
        destroyCarousel();

        if ($(this).hasClass("he_IL")) {
          localStorage.setItem("pl_language", "he_IL");
          $(".container-slider").attr("dir", "rtl");
          lanchCarousel(true);
          getAllLabelText();
        } else if ($(this).hasClass("fr_FR")) {
          localStorage.setItem("pl_language", "fr_FR");
          $(".container-slider").attr("dir", "");
          lanchCarousel(false);
          getAllLabelText();
        }
      });
    });

    lanchCarousel();

    btnScroll(
      $("#realisations-carousel"),
      $(".real_previous_caro"),
      $(".real_next_caro")
    );
    btnScroll(
      $("#realisations-expert-carousel"),
      $(".real_expert_previous_caro"),
      $(".real_expert_next_caro")
    );
    btnScroll(
      $("#temoignages-carousel"),
      $(".testimonial_previous_caro"),
      $(".testimonial_next_caro")
    );
    btnScroll(
      $("#services-carousel"),
      $(".service_previous_caro"),
      $(".service_next_caro")
    );

    $("#datepicker").datepicker({
      showButtonPanel: true,
      minDate: 0,
      inline: true,
      beforeShow: function (input) {
        setTimeout(function () {
          var buttonPane = $(input)
            .datepicker("widget")
            .find(".ui-datepicker-buttonpane");

          var btn = $(
            '<button class="ui-state-default ui-priority-secondary ui-corner-all" type="button">Valider</button>'
          );
          btn.unbind("click").bind("click", function () {
            $(input).datepicker("hide");
          });

          btn.appendTo(buttonPane);
        }, 1);
      },
    });

    $("#timepicker").timepicker({
      show2400: true,
      timeFormat: "H:i",
      step: 30,
      minTime: "8:00am",
      maxTime: "6:30pm",
      disableTimeRanges: [
        ["9:00am", "10:00am"],
        ["3:30pm", "4:30pm"],
      ],
    });
  });
})(jQuery);
